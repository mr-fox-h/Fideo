#!/usr/bin/python3

from converter import Converter
conv = Converter()
import_video = input("Import video: ")
export_video = input("Export video: ")
codec = input("Codec (Export): ")
width = input("Width (Export): ")
height = input("Height (Export): ")
fps = input("FPS (Export): ")
info = conv.probe(import_video)
convert = conv.convert(import_video, export_video, {
    'format': 'mp4',
    'audio': {
        'codec': 'aac',
        'samplerate': 11025,
        'channels': 2
    },
    'video': {
        'codec': codec,
        'width': width,
        'height': height,
        'fps': fps
    }})

for timecode in convert:
    print(f'\rConverting ({timecode:.2f}) ...')
